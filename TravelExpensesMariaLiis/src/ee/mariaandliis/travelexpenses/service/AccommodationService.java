package ee.mariaandliis.travelexpenses.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.mariaandliis.travelexpenses.tavels.Accommodation;
import ee.mariaandliis.travelexpenses.tavels.Transport;

public class AccommodationService {


	public List<Accommodation> getAccommodationByTravelId(int travelId) {
		List<Accommodation> listOfAccommodations = new ArrayList<>();
		
		Connection connection = DBConnectionHandling.createConnection();
		String queryString = "SELECT * FROM employee.accommodation WHERE travel_id= " + travelId + " ;";
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(queryString);) {
			while (resultSet.next()) {
				Accommodation accommodation = new Accommodation (resultSet.getInt("accommodation_id"), 
				//resultSet.getDate("accommodation_start_date"),
				//resultSet.getDate("accommodation_end_date"), 
						resultSet.getString("accommodation_name"), 
				resultSet.getDouble("accommodation_price"), resultSet.getInt("travel_id"));
			
				listOfAccommodations.add(accommodation);	
			}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfAccommodations;
	}
	
	public Accommodation addAccommodation(Accommodation accommodation) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employee.accommodation "
					+ "(accommodation_id, accommodation_name, accommodation_price, travel_id) VALUES" + 
					"(" + accommodation.getAccommodationId()+" ,'" + accommodation.getAccommodationName() + "',"
							 + accommodation.getAccommodationPrice()+ ", " + accommodation.getTravelId() + ") ; ");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accommodation;
	}
}
