package ee.mariaandliis.travelexpenses.service;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.mariaandliis.travelexpenses.service.DBConnectionHandling;

import ee.mariaandliis.travelexpenses.tavels.Accommodation;
import ee.mariaandliis.travelexpenses.tavels.Expense;
import ee.mariaandliis.travelexpenses.tavels.Transport;
import ee.mariaandliis.travelexpenses.tavels.Travel;
import ee.mariaandliis.travelexpenses.service.TransportService;

public class TravelService {
	public List<Travel> getTravelByEmployeeId(BigInteger employeeId) {
		List<Travel> listOfTravels = new ArrayList<>();

		// public Travel getTravelById(Integer travelId) {
		Connection connection = DBConnectionHandling.createConnection();
		String queryString = "SELECT * FROM employee.travel WHERE employee_id= " + employeeId + " ;";
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(queryString);) {
			while (resultSet.next()) {
				Travel travel = new Travel(resultSet.getString("travel_from_location"),
						// resultSet.getDate("travel_start_date_time"),
						resultSet.getString("travel_to_location"),
						// resultSet.getDate("travel_end_date_time"),
						resultSet.getString("travel_cause"), resultSet.getDouble("travel_total_price"),
						resultSet.getInt("travel_id"), resultSet.getString("status"),
						BigInteger.valueOf(resultSet.getLong("employee_id")));

				TransportService transportService = new TransportService();
				travel.setTransportsList(transportService.getTransportByTravelId(travel.getTravelId()));

				AccommodationService accommodationService = new AccommodationService();
				travel.setAccommodationsList(accommodationService.getAccommodationByTravelId(travel.getTravelId()));

				ExpenseService expenseService = new ExpenseService();
				travel.setExpensesList(expenseService.getExpenseByTravelId(travel.getTravelId()));

				listOfTravels.add(travel);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfTravels;
	}

	public Travel getTravelByTravelId(int travelId) {
		Travel travel = null;
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement
						.executeQuery("SELECT * FROM employee.travel WHERE travel_id= " + travelId + " ;");) {
			while (resultSet.next()) {
				travel = new Travel(resultSet.getString("travel_from_location"),
						// resultSet.getDate("travel_start_date_time"),
						resultSet.getString("travel_to_location"),
						// resultSet.getDate("travel_end_date_time"),
						resultSet.getString("travel_cause"), resultSet.getDouble("travel_total_price"),
						resultSet.getInt("travel_id"), resultSet.getString("status"),
						BigInteger.valueOf(resultSet.getLong("employee_id")));

				TransportService transportService = new TransportService();
				travel.setTransportsList(transportService.getTransportByTravelId(travel.getTravelId()));

				AccommodationService accommodationService = new AccommodationService();
				travel.setAccommodationsList(accommodationService.getAccommodationByTravelId(travel.getTravelId()));

				ExpenseService expenseService = new ExpenseService();
				travel.setExpensesList(expenseService.getExpenseByTravelId(travel.getTravelId()));

				// travel.add(travel);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return travel;
	}

	public Travel addTravel(Travel travel) {
		// Travel travel = new Travel();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employee.travel "
					+ "(travel_from_location, travel_to_location, travel_cause, travel_total_price, status, employee_id) VALUES"
					+ "('" + travel.getTravelFrom() + "','" + travel.getTravelTo() + "','" + travel.getTravelCause()
					+ "'," + travel.getTotalPrice() + " ,'" + travel.getStatus() + "', " + travel.getEmployeeId()
					+ ") ; ");

			ResultSet resultSet = statement.executeQuery("SELECT MAX(travel_id) as travel_id FROM employee.travel");
			while (resultSet.next()) {
				travel.setTravelId(resultSet.getInt("travel_id"));
			}

			TransportService transportService = new TransportService();
			for (Transport transport : travel.getTransportsList()) {
				transport.setTravelId(travel.getTravelId());
				transportService.addTransport(transport);
			}
			AccommodationService accommodationService = new AccommodationService();
			for (Accommodation accommodation : travel.getAccommodationsList()) {
				accommodation.setTravelId(travel.getTravelId());
				accommodationService.addAccommodation(accommodation);
			}

			ExpenseService expenseService = new ExpenseService();
			for (Expense expense : travel.getExpensesList()) {
				expense.setTravelId(travel.getTravelId());
				expenseService.addExpense(expense);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return travel;
	}

//	public Travel deleteTravel(int travelId) {
//		Travel travel = null;
//		Connection connection = DBConnectionHandling.createConnection();
//		try (Statement statement = connection.createStatement();) {
//			statement.executeUpdate("DELETE FROM employee.travel  WHERE "
//					+ "(travel_from_location, travel_to_location, travel_cause, travel_total_price, status, employee_id)"
//					+ "('" + travel.getTravelFrom() + "','" + travel.getTravelTo() + "','" + travel.getTravelCause()
//					+ "'," + travel.getTotalPrice() + " ,'" + travel.getStatus() + "', " + travel.getEmployeeId()
//					+ ") ; ");
//
//	}}catch(SQLException e)
//	{// TODO Auto-generated catch block
//	e.printStackTrace();
//	}
//	DBConnectionHandling.closeConnection(connection);
//	return travel;
}