function addNewTravel(){
//event.preventDefault(
	newTravelJson = {
		  	"travelFrom": document.getElementById("travelFrom").value,
		    "travelTo": document.getElementById("travelTo").value,
		    "travelStart": null,
		    "travelEnd": null,
		    "travelCause": document.getElementById("travelCause").value,
		    "totalPrice": document.getElementById("totalPrice").value,
		    "status": document.querySelector('input[name="status"]:checked').value,
		    "employeeId":36501020417,
		    
		    "transportsList": [],
		    "accommodationsList": [],
		    "expensesList": []    
	};
	
	for (var i=0; i < document.getElementById("transportTableBodyInsert").children.length; i++){
		console.log(document.getElementById("transportTableBodyInsert").children[i]);	
		var currentTransport = {
			  "transportFrom" : document.getElementById("transportTableBodyInsert").children[i].children[0].children[0].value,
			  "transportTo": document.getElementById("transportTableBodyInsert").children[i].children[1].children[0].value,
	          "transportStartDate": null,
	          "transportEndDate": null,
	          "transportType": document.getElementById("transportTableBodyInsert").children[i].children[4].children[0].value,
	          "transportPrice": document.getElementById("transportTableBodyInsert").children[i].children[5].children[0].value
		}
		newTravelJson["transportsList"].push(currentTransport);
	}
	
	for (var i=0; i < document.getElementById("accommodationTableBodyInsert").children.length; i++) {
		console.log(document.getElementById("accommodationTableBodyInsert").children[i]);
		var currentAccommodation = {
				"accommodationName": document.getElementById("accommodationTableBodyInsert").children[i].children[0].children[0].value,
		        "accommodationStartDate": null,
		        "accommodationEndDate": null,
		        "accommodationPrice":document.getElementById("accommodationTableBodyInsert").children[i].children[3].children[0].value	
		}
		newTravelJson["accommodationsList"].push(currentAccommodation);
	}
	for (var i=0; i < document.getElementById("expenseTableBodyInsert").children.length; i++) {
		console.log(document.getElementById("expenseTableBodyInsert").children[i]);
		var currentExpense = {
				"expenseType": document.getElementById("expenseTableBodyInsert").children[i].children[0].children[0].value,
	            "expenseDate":document.getElementById("expenseTableBodyInsert").children[i].children[1].children[0].value,
	            "expensePrice": document.getElementById("expenseTableBodyInsert").children[i].children[2].children[0].value
		}
		newTravelJson ["expensesList"].push(currentExpense);
	}    
	
	console.log(newTravelJson);

	$.ajax({url: "http://localhost:8080/TravelExpensesMariaLiis2/rest/travels", 
		cashe: false,
		type: 'POST',
		data: JSON.stringify(newTravelJson),
		success: function (newTravelJson){
			console.log(newTravelJson.travelId)
			//location.reload();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		},
		contentType: "application/json; charset=utf-8"
	});
}


function addNewTransportRow() {
	$("#transportTableBodyInsert")
			.append(
					'<tr><td><input type="text" id = "transportFrom">'
							+ '</td><td><input type="text" id = "transportTo"></td><td><input type="datetime-local" ></td>'
							+ '<td><input type="datetime-local"></td><td><input type="text" id = "transportType"></td>'
							+ '<td><input type="number" id = "transportPrice"></td><td><button type="button" '
							+ 'onClick="addNewTransportRow()" > Add new </button></td></tr>');

}

function addNewAccommodationRow() {
	$("#accommodationTableBodyInsert")
	.append(
			        '<tr><td><input type="text" id=accommodationName></td><td><input type="date" id="accommodationStartDate"></td>'
							 + '<td><input type="date" id="accommodationEndDate"></td><td><input type="number" id="accommodationPrice"></td>'
							 +  '<td><button type="button" onClick= "addNewAccommodationRow()" > Add new </button></td></tr>');
	
}

function addNewExpenseRow() {
	$("#expenseTableBodyInsert")
	.append(
					'<tr><td> <select id= "expenseType"><option value="training">Training/seminar</option>'
							+ '<option value="transport">Transport</option><option value="customer">Customer related</option>'
							+ '<option value="represent">Representational</option></select> </td>'  								 							  								 
							+ '<td><input type="date" id="expenseDate"></td><td><input type="number" id="expensePrice"></td>'
							+ '<td><button type="button" onClick= "addNewExpenseRow()" > Add new </button></td></tr>');
}

