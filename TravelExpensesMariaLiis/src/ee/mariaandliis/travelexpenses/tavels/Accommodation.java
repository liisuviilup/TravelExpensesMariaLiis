package ee.mariaandliis.travelexpenses.tavels;

import java.time.LocalDate;

public class Accommodation {

	int accommodationId;
	String accommodationName;
	LocalDate accommodationStartDate;
	LocalDate accommodationEndDate;
	double accommodationPrice;
	int travelId;

	public Accommodation() {
	}

	public Accommodation(int accommodationID, String accommodationName,
			// LocalDate accommodationStartDate,
			// LocalDate accommodationEndDate,
			double accommodationPrice, int travelId) {
		this.accommodationId = accommodationID;
		this.accommodationName = accommodationName;
		// this.accommodationStartDate = accommodationStartDate;
		// this.accommodationEndDate = accommodationEndDate;
		this.accommodationPrice = accommodationPrice;
		this.travelId = travelId;
	}
	
	public int getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(int accommodationId) {
		this.accommodationId = accommodationId;
	}

	public String getAccommodationName() {
		return accommodationName;
	}

	public void setAccommodationName(String accommodationName) {
		this.accommodationName = accommodationName;
	}

	public LocalDate getAccommodationStartDate() {
		return accommodationStartDate;
	}

	public void setAccommodationStartDate(LocalDate accommodationStartDate) {
		this.accommodationStartDate = accommodationStartDate;
	}

	public LocalDate getAccommodationEndDate() {
		return accommodationEndDate;
	}

	public void setAccommodationEndDate(LocalDate accommodationEndDate) {
		this.accommodationEndDate = accommodationEndDate;
	}

	public double getAccommodationPrice() {
		return accommodationPrice;
	}

	public void setAccommodationPrice(double accommodationPrice) {
		this.accommodationPrice = accommodationPrice;
	}

	public int getTravelId() {
		return travelId;
	}

	public void setTravelId(int travelId) {
		this.travelId = travelId;
	}

	@Override
	public String toString() {
		return "Accommodation" + accommodationId + "" + accommodationName + "" + accommodationStartDate + ""
				+ accommodationEndDate + "" + accommodationPrice;

	}
}
