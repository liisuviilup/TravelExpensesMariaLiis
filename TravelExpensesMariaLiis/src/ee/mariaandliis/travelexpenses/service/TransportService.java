package ee.mariaandliis.travelexpenses.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.mariaandliis.travelexpenses.tavels.Transport;

public class TransportService {

	
	public List<Transport> getTransportByTravelId(int travelId) {
		List<Transport> listOfTransports = new ArrayList<>();
		
		Connection connection = DBConnectionHandling.createConnection();
		String queryString = "SELECT * FROM employee.transport WHERE travel_id= " + travelId + " ;";
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(queryString);) {
			while (resultSet.next()) {
				Transport transport = new Transport (resultSet.getInt("transport_id"), 
						resultSet.getString("transport_from_location"), 
				//resultSet.getDate("travel_start_date_time"), 
				resultSet.getString("transport_to_location"), 
				//resultSet.getDate("travel_end_date_time"), 
				resultSet.getString("transport_type"), resultSet.getDouble("transport_price"), 
				resultSet.getInt("travel_id"));
				
				listOfTransports.add(transport);
			}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfTransports;
	}
	public Transport addTransport(Transport transport) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employee.transport "
					+ "(transport_from_location, transport_to_location, transport_type, transport_price, travel_id) VALUES" + 
					"('" + transport.getTransportFrom() + "','"
							 + transport.getTransportTo() + "','" + transport.getTransportType()+ "',"
					+ transport.getTransportPrice() + " ," + transport.getTravelId() + "); " );

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return transport;
	}

}
