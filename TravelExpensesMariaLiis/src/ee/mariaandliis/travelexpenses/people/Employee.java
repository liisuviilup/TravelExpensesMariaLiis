package ee.mariaandliis.travelexpenses.people;

import java.math.BigInteger;

public class Employee {
	BigInteger employeeID;
	String firstName;
	String middleName;
	String lastName;
	String position;
	int departmentId;
	String roleLabel;
	
	
	public Employee(BigInteger employeeID, String firstName, String middleName, String lastName, String position, int departmentID){
		this.employeeID = employeeID;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.position = position;
		this.departmentId = departmentID;
			}

		
	public Employee() {
		
	}


	public BigInteger getEmployeeId() {
		return employeeID;
	}


	public void setEmployeeId(BigInteger employeeId) {
		this.employeeID = employeeId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getMiddleName() {
		return middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}


	public int getDepartment() {
		return departmentId;
	}


	public void setDepartment(int departmentID) {
		this.departmentId = departmentID;
	}

	
	public String getRoleLabel() {
		return roleLabel;
	}


	public void setRoleLabel(String roleLabel) {
		this.roleLabel = roleLabel;
	}


	// BigDecimal employeeID, String firstName, String lastName, String position, String department, 
	@Override
	public String toString() {
		return "Employee" + firstName + ", " + middleName + ", " + lastName + ", " + position + ", " + departmentId + ", " + roleLabel ;
	}
}
