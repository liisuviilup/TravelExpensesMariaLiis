package ee.mariaandliis.travelexpenses.tavels;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ee.mariaandliis.travelexpenses.tavels.Accommodation;
import ee.mariaandliis.travelexpenses.tavels.Transport;
import ee.mariaandliis.travelexpenses.tavels.Expense;

public class Travel {
	int travelId;
	String travelFrom;
	String travelTo;
	LocalDateTime travelStart;
	LocalDateTime travelEnd;
	String travelCause;
	double totalPrice;
	String status;
	BigInteger employeeId;

	List<Transport> transportsList = new ArrayList<>();
	List<Accommodation> accommodationsList = new ArrayList<>();
	List<Expense> expensesList = new ArrayList<>();

	public Travel() {
	}

	public Travel(String travelFrom, String travelTo, String travelCause, double totalPrice,
			String status, BigInteger employeeId) {
		this.travelFrom = travelFrom;
		this.travelTo = travelTo;
		// this.travelStart = travelStart;
		// this.travelEnd = travelEnd;
		this.travelCause = travelCause;
		this.totalPrice = totalPrice;
		this.status = status;
		this.employeeId = employeeId;
	}

	public Travel(String travelFrom, String travelTo, String travelCause, double totalPrice, int travelId,
			String status, BigInteger employeeId) {
		this.travelId = travelId;
		this.travelFrom = travelFrom;
		this.travelTo = travelTo;
		// this.travelStart = travelStart;
		// this.travelEnd = travelEnd;
		this.travelCause = travelCause;
		this.totalPrice = totalPrice;
		this.status = status;
		this.employeeId = employeeId;
	}

	// Getter and setters:

	public String getTravelFrom() {
		return travelFrom;
	}

	public void setTravelFrom(String travelFrom) {
		this.travelFrom = travelFrom;
	}

	public String getTravelTo() {
		return travelTo;
	}

	public void setTravelTo(String travelTo) {
		this.travelTo = travelTo;
	}

	public LocalDateTime getTravelStart() {
		return travelStart;
	}

	public void setTravelStart(LocalDateTime travelStart) {
		this.travelStart = travelStart;
	}

	public LocalDateTime getTravelEnd() {
		return travelEnd;
	}

	public void setTravelEnd(LocalDateTime travelEnd) {
		this.travelEnd = travelEnd;
	}

	public String getTravelCause() {
		return travelCause;
	}

	public void setTravelCause(String travelCause) {
		this.travelCause = travelCause;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTravelId() {
		return travelId;
	}

	public void setTravelId(int travelId) {
		this.travelId = travelId;
	}

	public BigInteger getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(BigInteger employeeId) {
		this.employeeId = employeeId;
	}

	public List<Transport> getTransportsList() {
		return transportsList;
	}

	public void setTransportsList(List<Transport> transportsList) {
		this.transportsList = transportsList;
	}

	public List<Accommodation> getAccommodationsList() {
		return accommodationsList;
	}

	public void setAccommodationsList(List<Accommodation> accommodationsList) {
		this.accommodationsList = accommodationsList;
	}

	public List<Expense> getExpensesList() {
		return expensesList;
	}

	public void setExpensesList(List<Expense> expensesList) {
		this.expensesList = expensesList;
	}

	@Override
	public String toString() {
		return "Travel" + travelId + "" + travelFrom + "" + travelTo + "" + travelStart + "" + travelEnd + ""
				+ travelCause + "" + totalPrice + "" + status;
	}
}
