package ee.mariaandliis.travelexpenses.people;

import java.math.BigDecimal;

public class Department {
	int departmentId;
	String departmentName;
	BigDecimal managerId;

	public Department(int departmentId, String departmentName, BigDecimal managerId) {
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.managerId = managerId;
	}
	
	public Department (){
		
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public BigDecimal getManagerId() {
		return managerId;
	}

	public void setManagerId(BigDecimal managerId) {
		this.managerId = managerId;
	}
	
	public String toString() {
		return "Department" + departmentId + "" + departmentName + "" + managerId;
	}
			}
