package ee.mariaandliis.travelexpenses.tavels;

public enum TransportType  {
	BUS("buss"), PLANE("lennuk"), TRAIN("rong"), FERRY("laev"), NONE("ei kasutanud");
	private String description;

	private TransportType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}
}
