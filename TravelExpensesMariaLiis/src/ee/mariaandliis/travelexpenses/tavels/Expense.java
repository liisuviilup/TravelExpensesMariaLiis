package ee.mariaandliis.travelexpenses.tavels;

import java.sql.Date;

public class Expense {

	int expenseId;
	String expenseType;
	double expensePrice;
	Date expenseDate;
	int travelId;
	
	public Expense() {
	}
	
	public Expense(int expenseId, String expenseType, Date expenseDate, double expensePrice, int travelId){
		this.expenseId = expenseId;
		this.expenseType = expenseType;
		this.expenseDate = expenseDate;
		this.expensePrice = expensePrice;
		this.travelId = travelId;
		
	}

	public int getExpenseId() {
		return expenseId;
	}

	public void setExpenseId(int expenseId) {
		this.expenseId = expenseId;
	}

	public String getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}

	public int getTravelId() {
		return travelId;
	}

	public void setTravelId(int travelId) {
		this.travelId = travelId;
	}

	public double getExpensePrice() {
		return expensePrice;
	}

	public void setExpensePrice(double expensePrice) {
		this.expensePrice = expensePrice;
	}

	public Date getExpenseDate() {
		return expenseDate;
	}

	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}

	@Override 
	 public String toString() {
	 return "Expense" + expenseId +""+ expenseType +""+ expenseDate +"" + expensePrice; 
	}
	
}
