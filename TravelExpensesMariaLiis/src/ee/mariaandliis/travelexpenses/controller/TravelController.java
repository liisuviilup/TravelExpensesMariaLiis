package ee.mariaandliis.travelexpenses.controller;


import java.math.BigInteger;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.mariaandliis.travelexpenses.service.TravelService;
import ee.mariaandliis.travelexpenses.tavels.Travel;


@Path("/travels")
public class TravelController {
	TravelService travelService = new TravelService();
	@GET
	@Path("/employee/{employee_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Travel> getTravelByEmployeeId(@PathParam("employee_id") BigInteger employeeId) {
		List<Travel> listOfTravels = travelService.getTravelByEmployeeId(employeeId);
		return listOfTravels;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Travel addTravel(Travel travel) {
		return travelService.addTravel(travel);
	}
	
	@GET
	@Path("/{travel_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Travel getTravelById(@PathParam("travel_id") int travelId) {
		return travelService.getTravelByTravelId(travelId);
	}
//	@GET
//	@Path("/name/{name}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Continent getCountryByName(@PathParam("name") String name) {
//		return continentService.getContinentByName(name);
//	}
}
