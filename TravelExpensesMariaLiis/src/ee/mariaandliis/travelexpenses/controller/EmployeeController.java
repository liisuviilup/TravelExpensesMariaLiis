package ee.mariaandliis.travelexpenses.controller;

import java.math.BigInteger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.mariaandliis.travelexpenses.people.Employee;
import ee.mariaandliis.travelexpenses.service.EmployeeService;

@Path("/employees")
public class EmployeeController {
	EmployeeService employeeService = new EmployeeService();
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<Continent> getContinents() {
//		List<Continent> listOfContinents = continentService.getAllContinents();
//		return listOfContinents;
//	}
	@GET
	@Path("/{employee_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee getEmployeeById(@PathParam("employee_id") BigInteger employee_id) {
		return employeeService.getEmployeeById(employee_id);
	}
//	@GET
//	@Path("/name/{name}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Continent getCountryByName(@PathParam("name") String name) {
//		return continentService.getContinentByName(name);
//	}

}
