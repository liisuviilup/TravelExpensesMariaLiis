package ee.mariaandliis.travelexpenses.service;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ee.mariaandliis.travelexpenses.people.Employee;

public class EmployeeService {
	public Employee getEmployeeById(BigInteger employeeId) {
		Employee employee = null;
		Connection connection = DBConnectionHandling.createConnection();
		String queryString = "SELECT * FROM employee.employee WHERE employee_id= " + employeeId + " ;";
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(queryString);) {
			while (resultSet.next()) {
				employee = new Employee();
				employee.setEmployeeId(BigInteger.valueOf(resultSet.getLong("employee_id")));
				employee.setFirstName(resultSet.getString("first_name"));
				employee.setMiddleName(resultSet.getString("middle_name"));
				employee.setLastName(resultSet.getString("last_name"));
				employee.setPosition(resultSet.getString("position"));
				employee.setDepartment(resultSet.getInt("department_id"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employee;
	}
	// public Continent getContinentById(int continentId) {
	// Continent continent = null;
	// Connection connection = DBConnectionHandling.createConnection();
	// try (Statement statement = connection.createStatement();
	// ResultSet resultSet = statement.executeQuery(
	// "SELECT * FROM country_management.continent WHERE continent_id="
	// + continentId + ";");) {
	// while (resultSet.next()) {
	// continent = new Continent(resultSet.getInt("continent_id"),
	// resultSet.getString("name"));
	// }
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// DBConnectionHandling.closeConnection(connection);
	// return continent;
	// }
	// public Continent getContinentByName(String name) {
	// Continent continent = null;
	// Connection connection = DBConnectionHandling.createConnection();
	// try (Statement statement = connection.createStatement();
	// ResultSet resultSet = statement.executeQuery(
	// "SELECT * FROM country_management.continent WHERE name='"
	// + name + "';");) {
	// while (resultSet.next()) {
	// continent = new Continent(resultSet.getInt("continent_id"),
	// resultSet.getString("name"));
	// }
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// DBConnectionHandling.closeConnection(connection);
	// return continent;
	// }
}
