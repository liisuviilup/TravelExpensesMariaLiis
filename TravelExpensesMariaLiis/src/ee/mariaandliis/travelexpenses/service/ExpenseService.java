package ee.mariaandliis.travelexpenses.service;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.mariaandliis.travelexpenses.tavels.Accommodation;
import ee.mariaandliis.travelexpenses.tavels.Expense;
import ee.mariaandliis.travelexpenses.tavels.Transport;

public class ExpenseService {


	public List<Expense> getExpenseByTravelId(int travelId) {
		List<Expense> listOfExpenses = new ArrayList<>();
		
		Connection connection = DBConnectionHandling.createConnection();
		String queryString = "SELECT * FROM employee.expense WHERE travel_id= " + travelId + " ;";
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(queryString);) {
			while (resultSet.next()) {
				Expense expense = new Expense (resultSet.getInt("expense_id"), 
				resultSet.getString("expense_type"), resultSet.getDate("expense_date"),
				resultSet.getDouble("expense_price"), resultSet.getInt("travel_id"));
				
				listOfExpenses.add(expense);
			}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfExpenses;
	}
	public Expense addExpense(Expense expense) {
		//Transport transport = new Transport();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employee.expense "
					+ "(expense_type, expense_date, expense_price, travel_id) VALUES" + 
					"('" + expense.getExpenseType() + "','"
							 + expense.getExpenseDate() + "'," + expense.getExpensePrice()+  ", " + expense.getTravelId() + ") ; ");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return expense;
	}

}
