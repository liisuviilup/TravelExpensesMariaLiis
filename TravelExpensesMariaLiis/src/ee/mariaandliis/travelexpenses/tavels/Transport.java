package ee.mariaandliis.travelexpenses.tavels;

import java.time.LocalDateTime;

public class Transport {
	int transportId;
	String transportFrom;
	String transportTo;
	LocalDateTime transportStartDate;
	LocalDateTime transportEndDate;
	String transportType;
	double transportPrice;
	int travelId;

	public Transport(){
	}
	
	public Transport(int transportId, String transportFrom, 
			//LocalDateTime transportStartDate, 
			String transportTo,
			//LocalDateTime transportEndDate, 
			String transportType, 
			double transportPrice, int travelId) {
		this.transportId = transportId;
		this.transportFrom = transportFrom;
		this.transportTo = transportTo;
		//this.transportStartDate = transportStartDate;
		//this.transportEndDate = transportEndDate;
		this.transportType = transportType;
		this.transportPrice = transportPrice;
		this.travelId = travelId;

	}
	
	public int getTransportId() {
		return transportId;
	}

	public void setTransportId(int transportId) {
		this.transportId = transportId;
	}

	public String getTransportFrom() {
		return transportFrom;
	}

	public void setTransportFrom(String transportFrom) {
		this.transportFrom = transportFrom;
	}

	public String getTransportTo() {
		return transportTo;
	}

	public void setTransportTo(String transportTo) {
		this.transportTo = transportTo;
	}

	public LocalDateTime getTransportStartDate() {
		return transportStartDate;
	}

	public void setTransportStartDate(LocalDateTime transportStartDate) {
		this.transportStartDate = transportStartDate;
	}

	public LocalDateTime getTransportEndDate() {
		return transportEndDate;
	}

	public void setTransportEndDate(LocalDateTime transportEndDate) {
		this.transportEndDate = transportEndDate;
	}

	public String getTransportType() {
		return transportType;
	}

	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}

	public double getTransportPrice() {
		return transportPrice;
	}

	public void setTransportPrice(double transportPrice) {
		this.transportPrice = transportPrice;
	}
	public int getTravelId() {
		return travelId;
	}
	public void setTravelId(int travelId) {
		this.travelId = travelId;
	}
	
	
	@Override
	public String toString() {
		return "Transport" + transportId +"" + transportFrom +""+ transportTo +"" + transportStartDate +""+ transportEndDate +""+ transportType +""+ transportPrice;
	}
	
}
