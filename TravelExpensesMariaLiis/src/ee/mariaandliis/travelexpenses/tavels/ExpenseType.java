package ee.mariaandliis.travelexpenses.tavels;

public enum ExpenseType {
	SEMINAR("training or seminar"), CUSTOMER("customer expenses"), TRNSPORT("transport expenses"), OTHER("other expenses");
	private String description;

	private ExpenseType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}
}
